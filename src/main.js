import Vue from 'vue'
import App from './App.vue'
import 'primeflex/primeflex.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import "./assets/app.scss";


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
